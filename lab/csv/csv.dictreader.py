#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
with open('list1.txt') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        print(row[0], row[1])
# to use DictReader, txt must use titles on first line.

