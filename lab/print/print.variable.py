#!/usr/bin/env python
# -*- coding: utf-8 -*-

# 'var' is assigned to "hello world"
# in quotes.
# the print() function print 'var'
# without quotes.

var = "hello world" 
print(var)

