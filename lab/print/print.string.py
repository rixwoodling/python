#!/usr/bin/env python
# -*- coding: utf-8 -*-

# the print() function prints
# strings in quotes.
# parenthesis are required for python 3.

print("hello world")

