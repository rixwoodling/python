#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import stdout to python

import subprocess
output = subprocess.check_output('expressvpn status', shell=True)
output.strip()

if output.strip() == 'Not connected' :
    print("ok")
else :
    print("nope")
