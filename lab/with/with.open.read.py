#!/usr/bin/env python
# -*- coding: utf-8 -*-

# with...as assigns .txt file to 'data'.
# the read() method is added to 'data'
# a the the file is printed.

with open('wtf.txt','r') as data:
    print(data.read())
    