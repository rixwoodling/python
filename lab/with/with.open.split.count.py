#!/usr/bin/env python
# -*- coding: utf-8 -*-

# word is assigned to string "foxtrot"
# with .txt file as variable 'file',
# a 'for' loop runs through 'file'
# assigning all words to 'words'
# the file is parsed for "foxtrot"
# and a number is printed.

word = "foxtrot"

with open('wtf.txt','r') as file:
    for line in file:
        words = line.split()
    count = words.count(word)
    print(count)
    