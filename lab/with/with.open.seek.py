#!/usr/bin/env python
# -*- coding: utf-8 -*-

# 'with...as' assigns .txt file to 'data'.
# the seek() method offsets starting point.
# the read() method prints the last line
# in this example with variable of 14.

with open('wtf.txt','r') as data:
    data.seek(14)
    print(data.read())
    