#!/usr/bin/env python
# -*- coding: utf-8 -*-

# 'word' is assigned to string "tango"
# 'k' is assigned to integer 0
# with .txt file as variable 'file',
# a for loop runs through 'file'
# assigning all words to 'words'
# words are counted that contain "tango"
# and printed.

word = "tango"
k = 0

with open('wtf.txt','r') as file:
    for line in file:
        words = line.split()
        for i in words:
            if(i == word):
                k = k + 1
print(k)                
