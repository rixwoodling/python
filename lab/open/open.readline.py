#!/usr/bin/env python
# -*- coding: utf-8 -*-

# data is assigned to open .txt file as
# read-only.
# readline method prints each line
# of the file.

data = open('abc.txt','r')
print(data.readline()),
print(data.readline()),
print(data.readline())

