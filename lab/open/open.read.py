#!/usr/bin/env python
# -*- coding: utf-8 -*-

# 'file' is assigned to open .txt file as
# read-only.
# the read method of the file is printed.

file = open('abc.txt','r')
print(file.read())

