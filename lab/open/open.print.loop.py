#!/usr/bin/env python
# -*- coding: utf-8 -*-

# data is assigned to open .txt file as
# read-only.
# the for loop runs through
# and prints each line in file.

data = open('abc.txt','r')
for i in data:
    print(i),
    
    