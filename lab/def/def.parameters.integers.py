#!/usr/bin/env python
# -*- coding: utf-8 -*-

# example 0
# define function as 3 parameters
def function(one, two, three):
    # print the sum of the 3 parameters
    print(one, two, three)
# call function with values
function(1, 2, 3)

# example 1
# define function parameters
def function(val1, val2):
    # when called, do this calculation
    return(val1 + val2)
# print calculation output of values
print(function(1, 2))

# example 2
# define function parameters
# when called, calculate x, y, and z
# must define 3 values and print calculation 
def items(x, y, z):
    return(x + y + z)
print(items(1, 2, 3))


