#!/usr/bin/env python
# -*- coding: utf-8 -*-

# example 1
# function returns message,
# explicitly prints function
def function(message):
    return(message)
print(function('Hello World'))

# example 2
# function prints message,
# function runs message
def function(message):
    print(message)
function('Hello World')

# example 3
# function prints 2 values,
# function is set to 2 strings
def function(val1, val2)
    print(val1 + " " + val2)
function("Hello", "World)

