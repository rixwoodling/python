#!/usr/bin/env python
# -*- coding: utf-8 -*-

# a function is defined by 'def',
# function name, parenthesis, and a colon.
# the function is called by name and parenthesis
# and prints "Hello World" when run.

def helloworld():
    print("Hello World")

helloworld()

