# coding: utf-8

hello = 'Hello' + ' '

def world():
    return 'World'

def main():
    print(hello + world())

main()
