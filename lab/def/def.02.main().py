#!/usr/bin/env python
# -*- coding: utf-8 -*-

# the function main() calls another
# function helloworld()
# and prints "Hello World".

def helloworld():
    print("Hello World")
    
def main():
    helloworld()
    
main()

