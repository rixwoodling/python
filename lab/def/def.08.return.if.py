#!/usr/bin/env python
# -*- coding: utf-8 -*-

# a simple function returning a value,
# used in an expression

def f(x):
    return x+x
y = f(3) + f(4)
if y == 25:
    print('Hello World')

