#!/usr/bin/env python
# -*- coding: utf-8 -*-

# class hello passes
class hello:
    pass
    
# hello class is assigned to 'a'
# print 'a', which passes
a = hello()
print(a)

