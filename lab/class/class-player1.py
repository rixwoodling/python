#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

class Player:
    def  __init__(self,gender,coins):
        self.gender = gender
        self.coins = coins

gender = random.choice('male','female')
coins = random.randint(0,10)

player = Player(gender,coins)

print("You are a " + player.gender),
print("and you have " + str( player.coins) + " coins.")

