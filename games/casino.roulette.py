import random

nums = range(37)
bank = 10**10
games = 3000
factor=1
bet_amount=100
next_bet = None
parity_in_row ={"odd":0, "even":0}

for i in xrange(games):
    num = nums[random.randint(0,36)]

    if next_bet == "odd":
        if num % 2 == 1:
            bank += factor*bet_amount
            factor = 1
            next_bet = None
        else:
            bank -= factor*bet_amount
            factor *= 2
    elif next_bet == "even":
        if num % 2 == 0:
            bank += factor*bet_amount
            factor = 1
            next_bet = None
        else:
            bank -= factor*bet_amount
            factor *= 2


    if num > 0 and num % 2 == 0:
        parity_in_row["even"] += 1
        parity_in_row["odd"] = 0
    elif num % 2 == 1:
        parity_in_row["odd"] += 1
        parity_in_row["even"] = 0
    else:
        parity_in_row ={"odd":0, "even":0}

    if parity_in_row["odd"] > 2:
        next_bet = "even"
    elif parity_in_row["even"] > 2:
        next_bet = "odd"
    else:
        next_bet = None

print bank