"Vinny"

Vinny is a Mafia Boss who wants his money, and he wants answers right now. He's going to ask you a series of 10 questions and you better not lie or he's going to get upset. Don't get Vinny upset.

After being roughed up a bit, you'd better answer some basic yes or no questions Vinny has for you. He wants to know you're telling the truth or else he's going to turn your head into Canoli.

0. Sound good?
yes / no
Y. (1) Smart move, kid. 
N. I'm bettin' 100 bucks Vinny turns you into a pin cushin. 
N. Why don't you try that smart talk with Vinny, punk.
N. With a mouth like that, Vinny will surely be make concrete art with your head.
N. Too bad. But now youz gotta answer Vinny anyway.

OK, tough guy, listen up...

1. Yesterday, were youz pickin up Pauly from Jimmy's place? 
yes / no
Y. (2)
N. Did youz pick up Pauly?
yes / no
Y. My boys saw youz there. Are you calling me a liar? 
yes / no
Y. I can't say it was nice knowin' youz. GAME OVER (opt) Was Pauly at Jimmy's?
N. I'm going to ask you again, did you pick up Pauly?
yes / no
N. Youz fuckin' with me here?
yes / no
N. I'm going to ask you again, did you pick up Pauly?
Y. Kid, you just made Vinny very upset. GAME OVER

2. Did Pauly give you a bag full of money?

yes / no

3. Do you know anything about why Pauly was over at Jimmy's?

4. Do you have the money?
yes / no
Y. CAn you tell us 

5. Does Pauly have the money?

6. Does Jimmy have the money?


Choose your own adventure type game.
Random Death Scenes
One Storyline
Multiple answers
Fact Checking and dead ends
10 Questions

1. Where's my money?
A1) It's with Bobby. (Wrong Answer - Go to Question #2b)
A2) It's with Jimmy. (Right Answer - Go to Question #2)
A4) I'll never talk! (Wrong Answer - Go to random death response)

2. Where's Jimmy?
A1) With Tony. (Right Answer - Go to Question #3)
A2) With Franky. (Wrong Answer - "Franky was whacked yesterday. Let me show you what I mean.")
A3) With your money! (Wrong Answer - Go to random death response)

2b. Where's Bobby?
A1) Bobby took the money to Alphonso. (Wrong Answer - "We all know Alphonso is dead. Just like you.")
A2) With Lorenzo. (Wrong Answer - "Lorenzo was whacked yesterday. This is a dead end for you.")
A3) Go find him yourself! (Wrong Answer - Go to random death response)

3. What makes Jimmy and Tony stupid enough to steal my money?
A) They paid off a guy called Pauly. (Wrong Answer - Go to Question #3b)
B) They're taking the money to Rocco in the morning. (Right Answer - Go to Question #4)
C) They wanted to buy you a pretty dress! (Wrong Answer - Go to random death response)

3b. Which Pauly has my money? 
A) Pauly Ricci. He owns the marina. (Wrong Answer - "Can't be. We whacked him already.")
B) Pauly Mancini. He owns the casino. (Wrong Answer - "Can't be. He's dead. I've been too nice thus far."
C) You know, Pauly want a cracker? (Wrong Answer - Go to random death response)

4. Who is this Rocco character?
A) Jimmy and Tony are investing with this drug dealer tonight. (4b)
B) Rocco is being paid to take you out of the picture. (5)
C) I forgot already! (Wrong Answer - Go to random death response)

4b. Where does this investment take place?
A) At Vito's place. (Wrong Answer - Go to Question #4c)
B) At some warehouse downtown. ("Wrong Answer - Go to Question #4d)  
C) At your mother's house! (Wrong Answer - Go to random death response)

4c. 
A)
B) All I know
C) 

4d. 
A)
B)
C)

5. And how did you acquire this information? 
A) Though Donny. (Right Answer - Go to Question #6)
B) Jimmy and Tony wanted me in on the job. ("Wrong Answer - Go to Question #5d)
C) From a Chinese fortune cookie! (Wrong Answer - Go to random death response)

5b. And what did you tell these clowns? 
A) I thought they would kill me if I said no. (Wrong Answer - "What a predicament you're in!")
B) I told them I was out. (Wrong Answer - "Nice try, kid.")
C) I thought it was a good idea.

6. Are you accusing my brother of putting a hit on me?
A) He's upset you took over the family business. (Wrong Answer - "We don't have a family business.")
B) It's not just Donny. Other people want Donny on top. (Right Answer - Go to Question #7)
C) He often calls you his little sister. (Wrong Answer - Go to random death response)

7. Where is this deal being made?
A) At this restaurant called Giovanni's. (Wrong Answer - "I thought you said in the morning. You truly think I'm stupid!")
B) At this restaurant called Giovanni's. (Right Answer - Go to Question #9)
C) Nowhere where you'll be! (Wrong Answer - Go to random death response)

8. What's your part in all this?
A) 
B)
C) 

9. 
A)
B)
C)

10-A. Last Question. What is Vinny's favorite dish? 
A) Pasta Fettucini.
B) 
C) 

10-B. Last Question. What 


Death Responses
"Hurt him." - Professional boxer knocks you dead to the ground. 
"You upset the family." - 
"I am a little disappointed...a little bit."
"You take me for a clown?"
"Wrong answer!" - Shoots you with a gun.
"Ever play Russian Roulette with a clip?"
"You make less sense than a dirty penny." 
"It's getting a bit chilly in here."

Win Responses
"Have some lasagna, kid. You did good."
"Vinny forgives you, this time."
"OK, kid. You passed the test."
"Thanks for playing, kid. You're free to go."







