import random
import sys
from time import sleep

BOUNDS = (1, 9)
TRIES_ALLOWED = 8

the_number = random.randint(*BOUNDS)

words = ("\n\tThe Door of Death\n")
for char in words:
    sleep(0.1)
    sys.stdout.write(char)
    sys.stdout.flush()

print("There are 9 doors numbered %d to %d in this room." % BOUNDS)
print("One of these numbered doors kills those who open it.")
print("You must open all other doors to proceed.\n")

for tries in range(TRIES_ALLOWED):
    guess = int(input("\nOpen a door: "))

    if guess > the_number:
        print("\nThe door you must fear is of a lower number.")
    elif guess < the_number:
        print("\nThe door you must fear is of a higher number.")
    else:
        print("\nYou opened the door of death!")
        print("You die!")
        break
else:
    print("\nDoor number %d is the Door of Death. You are truly fortunate." % (the_number))

# Admittedly a contorted way to write a statement that works in both Python 2 and 3...
try:
    input("Press the enter key to enter the next room.")
except:
    pass