import random

def play_game():

    # Much nicer than int(float(random.randrange(0,101)))
    computer = random.randint(0, 101)
    count = 0

    # Keep looping until we return
    while True:

        count += 1
        user = int(raw_input('Please guess a number: '))

        if user < computer:
            print 'too low!'
        elif user > computer:
            print 'too high!'
        else:
            print 'You win!'
            print 'It took you {} tries to get the right answer!'.format(count) 
            return  # Leave play_game

def main():

    print 'Welcome!'

    while True:    
        play_game()

        play_again = raw_input('Play again? y/n: ') == 'y'
        if not play_again:
            return  # Leave main

main()