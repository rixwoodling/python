import random

BOUNDS = (1, 1000)
TRIES_ALLOWED = 10

the_number = random.randint(*BOUNDS)

print("\n\tThe Room of the Magic Number\n")
print("In this room, a man sits at a table. ")
print("I'm thinking of a number between %d and %d." % BOUNDS)
print("You have 10 guesses.\n")

for tries in range(TRIES_ALLOWED):
    guess = int(input("Take a guess: "))

    if guess > the_number:
        print("Lower...")
    elif guess < the_number:
        print("Higher...")
    else:
        print("You are very lucky! The number was %d" % (the_number))
        print("It only took you %d tries!\n\n" % (tries + 1))
        print("You may proceed to the next room.")
        break
else:
    print("You have failed! A large ax takes your head and you die.\n\n")

# Admittedly a contorted way to write a statement that works in both Python 2 and 3...
try:
    input("Press the enter key to enter the next room.")
except:
    pass