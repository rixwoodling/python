#!/usr/bin/python
# -*- coding: utf-8 -*-

#2

import random, os
from random import shuffle

door = ['1', '2', '3', '4']
random.shuffle(door)
a = door[0]
b = door[1]
c = door[2]
d = door[3]

a_state = 0
b_state = 0
c_state = 0
d_state = 0

a_show = "█"
b_show = "█"
c_show = "█"
d_show = "█"

a_message = "You chose door number "+a+"."
b_message = "You chose door number "+b+"."
c_message = "You chose door number "+c+"."
d_message = "You chose door number "+d+"."

x_message = "You already chose this door. Try again."
no_message = ""
invalid_message = "That's not a valid choice."

print "\n\t" + "Behind each door below is a number 1-4." 
print "\t" + "Make five points without going over." 
print ""
print "\t\t" + "█ █ █ █"
print "\t\t" + "a b c d"

game = 0
my_score = 0
message = no_message

while (game == 0):    

    os.system('clear')

    print "\n\t" + "Behind each door below is a number 1-4." 
    print "\t" + "Make five points without going over." + "\n"
    
    print "\t\t" + a_show, b_show, c_show, d_show 
    print "\t\t" + "a b c d"

    print "\n\t" + message
    letter = raw_input("\t" + "Choose a letter: ",)
    print "\n"

    if letter == "a":
        if a_state == 0 :                                                #open
            message = a_message
            my_score = my_score + int(a)
            a_state = a_state + 1
            a_show = a
        elif a_state == 1 :                                            #closed
            message = x_message
        else:
            print "brokun."
    elif letter == "b":
        if b_state == 0 :                                                #open
            message = b_message
            my_score = my_score + int(b)
            b_state = b_state + 1
            b_show = b
        elif b_state == 1 :                                            #closed
            message = x_message
        else:
            print "brokun."        
    elif letter == "c":
        if c_state == 0 :
            message = c_message
            my_score = my_score + int(c)
            c_state = c_state + 1
            c_show = c
        elif c_state == 1 : 
            message = x_message
        else:
            print "brokun."        
    elif letter == "d":
        if d_state == 0 :
            message = d_message
            my_score = my_score + int(d)
            d_state = d_state + 1
            d_show = d
        elif d_state == 1 : 
            message = x_message
        else:
            print "brokun."       
    elif letter == "x":
        break
    else:
        message = invalid_message

    if my_score > 5 :
        game = 2
    elif my_score == 5 :
        game = 1

if game == 2 :

    os.system('clear')

    overpts = my_score - 5

    print "\n\n\t" + "YOU LOSE! You went over by %s." % overpts 
    print ""
    print "\t\t" + a_show, b_show, c_show, d_show 
    print "\t\t" + "a b c d"
    print ""

elif game == 1 :

    os.system('clear')

    print "YOU WIN!" 
    print ""
    print "\t\t" + a_show, b_show, c_show, d_show 
    print "\t\t" + "a b c d"

#    replay = raw_input("\n" + "Play agian? (y/n):",)
#   if replay == "y" :
#        game = 0
#   elif replay == "n" :
#        end(0)
#    else:
#        print "brokun."    

else:
    print "brokun."