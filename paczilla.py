#!/usr/bin/python
# -*- coding: utf-8 -*-
# paczilla v0.7

import os
import sys

# finds and lists all *_summary.csv files in a folder
# can be imported as a module or standalone

def paths():

# ->|
    b = []
    # list all directories ending with --r1
    for d in os.listdir( '.' ):
        if os.path.isdir( d ):
            if d.endswith( '--r1' ):
                b.append( d )
# ->|
    n = 1
    c = 0
    r0 = []; r1 = []; r2 = []; r3 = []; avg = []

    # for each --r1 directory
    for d in b:
        while c < 3:
            d = d[:-1] + str( n )
            for f in os.listdir( d ):

                # for each file ending in _summary.csv
                if f.endswith( "_summary.csv" ):
                    f = os.path.join( d,f )
                    with open( f,'r' ) as file:

                        #
                        for row in file:
                            if c == 0:
                                name_column = row.split( ',' )[0]
                                r0.append( name_column )
                            if n == 1:
                                avg_column = row.split( ',' )[3]
                                r1.append( avg_column )
                                r1[0] = 'r' + str( n )
                            if n == 2:
                                avg_column = row.split( ',' )[3]
                                r2.append( avg_column )
                                r2[0] = 'r' + str( n )
                            if n == 3:
                                avg_column = row.split( ',' )[3]
                                r3.append( avg_column )
                                r3[0] = 'r' + str( n )
# --------->|
            n = n + 1
            c = c + 1
# ----->|
        a = 'avg'
        avg.append( a )
        for i in range( 1, len( r1 )):
            a = float( r1[i] ) + float( r2[i] ) + float( r3[i] )
            a = a / 3
            avg.append( '%.6f' % a )

        for x in range( 0, len( r1 )):
            m = open( 'myoutput.csv','a' )
            m.write( '%s,%s,%s,%s,%s\n' % ( r0[x],r1[x],r2[x],r3[x],avg[x] ))
            m.close()

if __name__ == "__main__":
    paths()
    #m = open( 'myoutput.csv','w' )
    #for o in paths():
    #    m.write( o )
    #m.close()

