#!/usr/bin/python
# -*- coding: utf-8 -*-

# functions to perform series of tests 
# that try and test programming puzzles

import random
from random import choice

def randomnote():
	# prints random key from list
	
	chro1 = [ 'C','G','D','A','E','B' ] 
	chro2 = [ 'F#','Db','Ab','Eb','Bb','F' ]
	chromatic = chro1 + chro2
	
	print( random.choice(chromatic) )

def chordorder():
	# determines which chord to print 
	# based on nonrepeating alphabetical order
	
	chord = ['D',['D#','Eb'],'F']
	note = chord[0]                 # D
	
	print(note),                    # D
	print(chord[1]),                # ['D#','Eb']
	print(chord[1][0]),             # D#
	print(chord[1][0][:1])          # D
	
	if chord[1][0][:1] == note:
		print( chord[1][1] )        # Eb
	else:
		print( chord[1][0] )        # D
	return('')
	
randomnote()
chordorder()

