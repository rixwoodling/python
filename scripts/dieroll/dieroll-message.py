#!/usr/bin/python
# -*- coding: utf-8 -*-

# rolls a die and outputs a message

import random 

roll = random.randint(1, 6) 
if roll == 1:
	print("You rolled a one.")
	exit(0)
elif roll == 2: 
	print("You rolled a two.")
	exit(0)
elif roll == 3:
	print("You rolled a three.")
	exit(0)
elif roll == 4: 
	print("You rolled a four.")
	exit(0)
elif roll == 5: 
	print("You rolled a five.")
	exit(0)
elif roll == 6: 
	print("You rolled a six.")
	exit(0)
else: 
	print("You rolled nothing.")
	exit(0)
	
	