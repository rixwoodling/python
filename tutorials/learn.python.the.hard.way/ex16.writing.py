from sys import argv

script, filename = argv

print "We're going to erase %r." % filename
print "If you don't want that, hit CTRL-C (^C)."
print "If you do what that, hit RETURN."

raw_input("?")

print "Opening the file..."
target = open(filename, 'w')
target.truncate()