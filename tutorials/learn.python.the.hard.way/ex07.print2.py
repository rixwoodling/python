print "Mary had a little lamb." #prints this sentence
print "Its fleece was white as %s." % 'snow' #prints using inline interpolation
print "And everywhere that Mary went." #prints this sentence
print "." * 10 #prints a "." 10 times

end1 = "C" #variable
end2 = "h" #variable
end3 = "e" #variable
end4 = "e" #variable
end5 = "s" #variable
end6 = "e" #variable
end7 = "B" #variable
end8 = "u" #variable
end9 = "r" #variable
end10 = "g" #variable
end11 = "e" #variable
end12 = "r" #variable

# if comma is removed (line 20), line 20 and 21 will appear on new lines
print end1 + end2 + end3 + end4 + end5 + end6, #Cheese
print end7 + end8 + end9 + end10 + end11 + end12 #Burger
# together with comma prints "Cheese Burger