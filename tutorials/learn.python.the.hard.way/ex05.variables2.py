name = "Rix"
age = 36
height = 68
weight = 160
eyes = 'magical'
teeth = 'sparkling'
hair = 'dreamy'

print "Let's talk about %s." % name
print "He's %r inches tall." % height
print "He's %r pounds heavy." % weight
print "Actually, that's not too heavy."
print "He's got %r eyes and %r hair." % (eyes, hair)
print "His teeth are usually %r depending on the coffee." % teeth

print "If I add %r, %r, and %r I get %r." % (age, height, weight, age + height + weight)