#!/usr/bin/env python
# -*- coding: utf-8 -*-

# http://www.practicepython.org/exercise/2014/03/05/05-list-overlap.html

# list all numbers from both lists only once

a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

for num in a:
    if num not in c:
        c.append(num)
for num in b:
    if num not in c:
        c.append(num)
print(sorted(c))

